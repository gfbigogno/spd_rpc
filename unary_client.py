import grpc
import unary_pb2_grpc as pb2_grpc
import unary_pb2 as pb2


class UnaryClient(object):
    """
    Client for gRPC functionality
    """

    def __init__(self):
        self.host = 'localhost'
        self.server_port = 50051

        # instantiate a channel
        self.channel = grpc.insecure_channel(
            '{}:{}'.format(self.host, self.server_port))

        # bind the client and the server
        self.stub = pb2_grpc.UnaryStub(self.channel)

    def get_url(self):
        """
        Client function to call the rpc for GetServerResponse
        """
        message = pb2.Message()

        try:
            arquivo = open("senha.txt", "w")
            senha = self.stub.GetServerResponse(message).message
            arquivo.write(senha)
        except grpc.RpcError as rpc_error:
            if rpc_error.code() == grpc.StatusCode.CANCELLED:
                return "CONEXAÇÃO COM O SERVIDOR CANCELADA"
            elif rpc_error.code() == grpc.StatusCode.UNAVAILABLE:
                return "CONEXAÇÃO COM O SERVIDOR INDISPONÍVEL"
            else:
                print(
                    f"Received unknown RPC error: code={rpc_error.code()} message={rpc_error.details()}")


if __name__ == '__main__':
    client = UnaryClient()
    result = client.get_url()
    print(f'{result}')
